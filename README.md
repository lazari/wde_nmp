# NMP stack built with Docker Compose

This is a basic NMP stack environment built using Docker Compose. It consists following:

* PHP-FPM 
* Nginx
* MySQL 
* PHPMyAdmin / Adminer
* Node.js

Your NMP stack is now ready!! You can access it via `http://localhost`.

## Configuration

This package comes with default configuration options. You can modify them by modify `.env` file in your root directory.

## PHP

The installed version of PHP is 8.1

#### Extensions

By default following extensions are installed.

* mysqli
* pdo_mysqsl
* mbstring
* zip
* intl
* gd
* xdebug
* opcache
* imagick
* bcmath

> If you want to install more extension, just update `./docker-compose/php/Dockerfile`. You can also generate a PR and we will merge if seems good for general purpose.
> You have to rebuild the docker image by running `docker-compose build` and restart the docker containers.

#### Connect via SSH

You can connect to web server using `docker exec` command to perform various operation on it. Use below command to login to container via ssh.

```shell
docker exec app ls -al
docker container exec -it php /bin/bash
docker container exec -it node /bin/bash
```
```shell
docker exec app composer -v
Composer version 2.0.9 2021-01-27 16:09:27
```

## Adminer

Adminer is configured to run on port 8080. Use following default credentials.

http://localhost:8080/  
username: root  
password: Password!01
